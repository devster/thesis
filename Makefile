destination = ~/Dropbox/research/daniele_tbc/dist

docx:
	pandoc -t docx -o ${destination}/main.docx main.tex # -s is implicit

odt:
	pandoc -t odt -o ${destination}/main.odt main.tex # -s is implicit

rtf:
	pandoc -t rtf -s -o ${destination}/main.rtf main.tex

pdf:
	cp main.pdf ${destination}/main.pdf

all: pdf rtf odt docx

# clean:
# 	latexmk -c
#
# clean-all:
# 	latexmk -C
#
# build:
# 	latexmk -interaction=nonstopmode -f -cd -${outputFormat} -synctex=1 -file-line-error main.tex # -shell-escape
#
# build-force:
# 	latexmk -gg

.PHONY: all dist clean docx odt rtf
